#!/bin/sh
curl -G https://overpass-api.de/api/interpreter --data-urlencode data@`dirname $0`/../overpass/milestones.ovp > milestones.csv || ( echo "WARNING retrieving milestones from Overpass API failed: network error" && exit 1)
# check if we got an error document
grep -q "<html" milestones.csv && (echo "WARNING retrieving milestones from Overpass API failed: invalid result"; cat milestones.csv; exit 2)
echo "Milestones were retrieved successfully."
exit 0

