package org.traffxml.gddkia.milestone;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.NavigableMap;
import java.util.Set;

import org.traffxml.gddkia.GddkiaDelay;
import org.traffxml.gddkia.GddkiaFeed;
import org.traffxml.gddkia.GddkiaMessage;
import org.traffxml.lib.milestone.Distance;
import org.traffxml.lib.milestone.Milestone;
import org.traffxml.lib.milestone.MilestoneList;

public class MilestoneChecker {

	public static void main(String[] args) {
		String input = null;
		String milestonePath = null;
		GddkiaFeed feed = null;
		MilestoneList milestones = null;

		/* parse arguments */
		if (args.length < 1) {
			printUsage();
			System.exit(1);
		}
		for (int i = 0; i < args.length; i++) {
			if ("-milestones".equals(args[i])) {
				milestonePath = getParam("milestones", args, ++i);
			} else
				input = args[i];
		}

		/* fetch the feed */
		if (input.startsWith("http://") || input.startsWith("https://")) {
			URL url;
			try {
				url = new URL(input);
				feed = GddkiaFeed.parseXml(url.openStream());
			} catch (MalformedURLException e) {
				System.err.println("Input URL is invalid. Aborting.");
				System.exit(1);
			} catch (IOException e) {
				System.err.println("Cannot read from input URL. Aborting.");
				System.exit(1);
			}
		} else {
			File inputFile = new File(input);
			try {
				feed = parseFile(inputFile);
			} catch (FileNotFoundException e) {
				System.err.println("Input file does not exist. Aborting.");
				System.exit(1);
			} catch (IllegalArgumentException e) {
				System.err.println(String.format("%s. Aborting.", e.getMessage()));
				System.exit(1);
			}
		}
		
		/* load milestone data */
		if (milestonePath != null) {
			File milestoneFile = new File(milestonePath);
			if (!milestoneFile.exists()) {
				System.err.println(String.format("Milestone file %s does not exist", milestonePath));
				milestonePath = null;
			} else if (!milestoneFile.isFile()) {
				System.err.println(String.format("Milestone file %s is not a file", milestonePath));
				milestonePath = null;
			} else if (!milestoneFile.canRead()) {
				System.err.println(String.format("Milestone file %s is not readable", milestonePath));
				milestonePath = null;
			} else
				try {
					System.out.println(String.format("Using milestones from %s", milestonePath));
					milestones = new MilestoneList(new FileInputStream(milestonePath),
							'\t', ';', "@lat", "@lon", "ref", "distance", Distance.UNIT_KM);
				} catch (IOException e) {
					System.err.println("An error occurred while parsing milestone data; the list may be incomplete.");
					System.err.println(e);
				}
		}

		if (milestonePath == null) {
			try {
				System.out.println("Using built-in list of milestones");
				milestones = new MilestoneList(GddkiaMessage.class.getResourceAsStream("milestones.csv"),
						'\t', ';', "@lat", "@lon", "ref", "distance", Distance.UNIT_KM);
			} catch (IOException e) {
				System.err.println("An error occurred while parsing milestone data; the list may be incomplete.");
				System.err.println(e);
			}
		}

		/* examine the feed */
		if (feed == null) {
			System.err.println("No feed received. Aborting.");
			System.exit(1);
		}
		int newRoads = 0;
		int partialRoads = 0;
		int newFiles = 0;
		System.out.println(String.format("%d message(s) received.\n", feed.utr.length));
		for (GddkiaMessage message : feed.utr) {
			boolean hasNewMilestones = false;
			NavigableMap<Distance, Set<Milestone>> roadMilestones = milestones.get(message.nrDrogi);
			if ((roadMilestones == null) || roadMilestones.isEmpty()) {
				/* no milestones for this road number so far */
				System.out.println(String.format("Road number without milestones: %s", message.nrDrogi));
				hasNewMilestones = true;
				newRoads++;
			} else {
				/* we have milestones for this road number, see if endpoints are covered */
				for (Float kmpRaw : new Float[]{message.km, message.km + message.dl}) {
					Distance messageDist = new Distance(kmpRaw, Distance.UNIT_KM);
					boolean isMilestoneNew = true;
					for (Distance milestoneDist : new Distance[]{roadMilestones.floorKey(messageDist), roadMilestones.ceilingKey(messageDist)}) {
						if ((milestoneDist != null) && (messageDist.difference(milestoneDist, Distance.UNIT_KM) < 1.0f)) {
							isMilestoneNew = false;
							break;
						}
					}
					if (isMilestoneNew) {
						System.out.println(String.format("Unmapped milestone on road: %s", message.nrDrogi));
						hasNewMilestones = true;
						partialRoads++;
						break;
					}
				}
				// TODO figure out if we have nearby milestones
			}
			if (hasNewMilestones) {
				printMessage(message);
				boolean stored = storeMessage(message, feed.gen);
				if (stored)
					newFiles++;
				System.out.println();
			}
		}
		System.out.println(String.format("%d message(s) referring to new roads.", newRoads));
		System.out.println(String.format("%d message(s) referring to missing milestones.", partialRoads));
		System.out.println(String.format("%d new file(s) created.", newFiles));
		System.out.println("Done.");
	}

	private static String getParam(String param, String[] args, int pos) {
		if(pos >= args.length) {
			System.out.println("-" + param + " needs an argument.");
			System.exit(1);
		}
		return args[pos];
	}

	private static GddkiaFeed parseFile(File file) throws FileNotFoundException {
		if (!file.exists()) {
			throw new IllegalArgumentException("Input file does not exist");
		} else if (!file.isFile()) {
			throw new IllegalArgumentException("Input file is not a file");
		} else if (!file.canRead()) {
			throw new IllegalArgumentException("Input file is not readable");
		}
		return GddkiaFeed.parseXml(new FileInputStream(file));
	}

	private static void printMessage(GddkiaMessage message) {
		System.out.println(String.format("  From km %.3f", message.km));
		System.out.println(String.format("  To km %.3f", message.km + message.dl));
		System.out.println(String.format("  Voivodeship: %s", message.woj));
		System.out.println(String.format("  Section: %s", message.nazwaOdcinka));
		System.out.println(String.format("  Description: %s", (message.objazd !=null) ? message.objazd.replace('\n', ' ') : null));
		if ((message.geoLat != null) && (message.geoLong != null))
			System.out.println(String.format("  Link: https://www.openstreetmap.org?mlat=%.6f&mlon=%.6f", message.geoLat, message.geoLong));
	}

	private static void printUsage() {
		System.out.println("Arguments:");
		System.out.println("  -milestones <file>  Read milestone data from <file>");
		System.out.println("  <feed>              Check milestone information in <feed>");
		System.out.println();
		System.out.println("<feed> can refer to a local file or an http/https URL");
	}

	private static boolean storeMessage(GddkiaMessage message, Date gen) {
		String id = String.format("%s@%.3f,%.3f.xml", message.nrDrogi, message.km, message.dl);
		File file = new File(id);
		if (file.exists()) {
			System.out.println(String.format("  File %s already exists, nothing saved", id));
			return false;
		}
		try {
			PrintWriter out = new PrintWriter(file);
			out.printf("<utrudnienia gen=\"%1$tFT%1$tT%1$tz\">\n", gen);
			out.printf("  <utr>\n");
			out.printf("    <typ>%s</typ>\n", message.typ);
			out.printf("    <nr_drogi>%s</nr_drogi>\n", message.nrDrogi);
			out.printf("    <woj>%s</woj>\n", message.woj);
			out.printf("    <km>%.3f</km>\n", message.km);
			out.printf("    <dl>%.3f</dl>\n", message.dl);
			out.printf("    <geo_lat>%s</geo_lat>\n", message.geoLat == null ? "" : String.format("%.6f", message.geoLat));
			out.printf("    <geo_long>%s</geo_long>\n", message.geoLong == null ? "" : String.format("%.6f", message.geoLong));
			out.printf("    <nazwa_odcinka>%s</nazwa_odcinka>\n", message.nazwaOdcinka);
			out.printf("    <data_powstania>%1$tFT%1$tT%1$tz</data_powstania>\n", message.dataPowstania);
			out.printf("    <data_likwidacji>%1$tFT%1$tT%1$tz</data_likwidacji>\n", message.dataLikwidacji);
			out.printf("    <objazd>\n");
			out.printf("      %s\n", message.objazd);
			out.printf("    </objazd>\n");
			out.printf("    <objazd_mapy>\n");
			out.printf("      <mapa/>\n");
			out.printf("    </objazd_mapy>\n");
			out.printf("    <rodzaj>\n");
			for (String poz : message.rodzaj)
				out.printf("      <poz>%s</poz>\n", poz);
			out.printf("    </rodzaj>\n");
			if (message.skutki != null)
				out.printf("    <skutki>%s</skutki>\n", message.skutki);
			else
				out.printf("    <skutki/>\n");
			if (message.ogrNosnosc != null)
				out.printf("    <ogr_nosnosc>%.1f</ogr_nosnosc>\n", message.ogrNosnosc);
			else
				out.printf("    <ogr_nosnosc/>\n");
			if (message.ogrNacisk != null)
				out.printf("    <ogr_nacisc>%.1f</ogr_nacisc>\n", message.ogrNacisk);
			else
				out.printf("    <ogr_nacisc/>\n");
			if (message.ogrSkrajniaPozioma != null)
				out.printf("    <ogr_skrajnia_pozioma>%.1f</ogr_skrajnia_pozioma>\n", message.ogrSkrajniaPozioma);
			else
				out.printf("    <ogr_skrajnia_pozioma/>\n");
			if (message.ogrSkrajniaPionowa != null)
				out.printf("    <ogr_skrajnia_pionowa>%.1f</ogr_skrajnia_pionowa>\n", message.ogrSkrajniaPionowa);
			else
				out.printf("    <ogr_skrajnia_pionowa/>\n");
			if (message.ogrSzerokosc != null)
				out.printf("    <ogr_szerokosc>%.1f</ogr_szerokosc>\n", message.ogrSzerokosc);
			else
				out.printf("    <ogr_szerokosc/>\n");
			if (message.ogrPredkosc != null)
				out.printf("    <ogr_predkosc>%d</ogr_predkosc>\n", message.ogrPredkosc);
			else
				out.printf("    <ogr_predkosc/>\n");
			if (message.ruchWahadlowy != null)
				out.printf("    <ruch_wahadlowy>%b</ruch_wahadlowy>\n", message.ruchWahadlowy);
			else
				out.printf("    <ruch_wahadlowy/>\n");
			if (message.ruch2Kierunkowy != null)
				out.printf("    <ruch_2_kierunkowy>%b</ruch_2_kierunkowy>\n", message.ruch2Kierunkowy);
			else
				out.printf("    <ruch_2_kierunkowy/>\n");
			if (message.drogaZamknieta != null)
				out.printf("    <droga_zamknieta>%b</droga_zamknieta>\n", message.drogaZamknieta);
			else
				out.printf("    <droga_zamknieta/>\n");
			out.printf("    <czasy_oczekiwania>\n");
			for (GddkiaDelay czasOczekiwania : message.czasyOczekiwania) {
				out.printf("      <czas_oczekiwania>\n");
				out.printf("        <kierunek>%s</kierunek>\n", czasOczekiwania.kierunek);
				out.printf("        <czas_od>%02d:%02d</czas_od>\n",
						czasOczekiwania.czasOd.getHour(),
						czasOczekiwania.czasOd.getMinute());
				out.printf("        <czas_do>%02d:%02d</czas_do>\n",
						czasOczekiwania.czasDo.getHour(),
						czasOczekiwania.czasDo.getMinute());
				out.printf("        <pn_pt>%02d:%02d</pn_pt>\n",
						czasOczekiwania.pnPt.getSeconds() / 3600,
						(czasOczekiwania.pnPt.getSeconds() % 3600) / 60);
				out.printf("        <so>%02d:%02d</so>\n",
						czasOczekiwania.so.getSeconds() / 3600,
						(czasOczekiwania.so.getSeconds() % 3600) / 60);
				out.printf("        <ni>%02d:%02d</ni>\n",
						czasOczekiwania.ni.getSeconds() / 3600,
						(czasOczekiwania.ni.getSeconds() % 3600) / 60);
				out.printf("      </czas_oczekiwania>\n");
			}
			out.printf("    </czasy_oczekiwania>\n");
			out.printf("  </utr>\n");
			out.printf("</utrudnienia>\n");
			out.close();
			System.out.println(String.format("  Stored as file: %s", id));
			return true;
		} catch (Exception e) {
			System.out.println(String.format("  Unable to write file %s: %s", id, e.toString()));
			return false;
		}
	}
}
