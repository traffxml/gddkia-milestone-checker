# OSM milestone verification tool for Poland

This tool uses traffic reports from the Polish road directorate GDDKiA to verify completeness of milestones (`highway=milestone`) in OSM.

When run, it retrieves the current traffic report from https://www.archiwum.gddkia.gov.pl/dane/zima_html/utrdane.xml and examines its location data. For both the start (`km` element) and end (sum of `km` and `dl` elements) of each event, it checks if there is a matching milestone in OSM data. If not, this is reported.

Rules for the check are as follows:
* Milestones are considered a match if they are nominally **less than** 1 km from the location indicated by `km` and `km+dl` (as milestones are typically spaced 1 km apart).
* The road number must exactly match one of the values in the `ref=*` tag of the milestone. For example, `S8f` will match `ref=S8;S8f` but not `ref=S8`.
* Some messages may specify WGS84 coordinates for a reference point. This is currently not used for comparison.

If one or two of the milestones do not have a match in OSM, a message is printed and the XML data for the message is stored in a file in the working directory, unless a file by that name already exists. The naming convention for files is `ref@km+dl.xml`.

## Running locally

If you want to run the check locally:

* Clone the repository and build the tool, or grab the jar with dependencies from one of the latest CI artifacts (be sure to look for them in the `build` job, which runs only on non-scheduled pipelines).
* Retrieve the latest OSM milestone data from Overpass. On a Unix-like system (for Windows users, Cygwin should do), run `src/shell/milestones.sh`, which will place a file named `milestones.csv` in the current directory.
* Change to the directory where you would like the traffix reports to be stored.
* Run the following command line (instead of `/path/to`, use the actual path to the respective files):

    java -cp /path/to/gddkia-milestone-checker-0.0.1-SNAPSHOT-jar-with-dependencies.jar org.traffxml.gddkia.milestone.MilestoneChecker -milestones /path/to/milestones.csv https://www.archiwum.gddkia.gov.pl/dane/zima_html/utrdane.xml

You can skip downloading the latest milestone data and omit the `-milestone` tag. In this case, the tool will use its bundled copy of milestone data. In this case, be aware that you are not working on the latest data.

## GitLab CI runs

On GitLab, pipelines are set up to run this tool four times a day, at 01:30, 09:00, 13:30 and 18:00 CET/CEST. This should return a good overview of various reports: work during the night, work during the daytime, as well as the morning and evening rush hours.

At the start of each run, milestone data for Poland is retrieved from Overpass API, ensuring the tool uses the most recent data. Should retrieval fail, the tool uses built-in milestone data (which may be outdated).
Files collected during each run are stored as CI artifacts. As artifacts are cached between runs, you only need to check the latest artifacts to find out if a report for a certain location has ever been stored. This policy may change as files accumulate.

## Working with the results

### Kilometric points in Poland

The national Polish road network was reorganized in 2001, and kilometric points on most roads seem to have been renumbered at the time, starting with km 0 at one end of the road and counting up from there.

Where a stretch of route is shared by two roads, kilometric points refer to the higher-order road or (between two roads of the same order), the road with the lower number. Counting is stopped for the other road and resumes where it left off at the end of the shared stretch.

When roads are re-routed, counting resumes at km 0 at the start of the new section. That stretch is then identified by a suffix, e.g. 69c for a rerouted stretch of road 69. The other parts keep their numbering.

When a national road is upgraded to an expressway without changing the route, the original kilometric point numbering is often (but not always) kept. In many places, the first stretches of a new expressway are bypasses around towns, replacing the old national road going through the center. They usually receive a suffix after their ref and start at km 0. Suffixes are also used when an expressway is constructed in several sections. Sometimes these sections are renumbered when the “missing” sections are opened to traffic.

Sometimes, numbering is continued throughout multiple road classes. For example, national road 2 starts at the German border with a stretch called 2b, which after some 3 km becomes A2 but kilometric point numbering continues, still indicating distance from the German border at Świecko. At Warsaw-Konotopa, A2 becomes S2, but kilometric points still indicate distance from Świecko. This continues beyond the end of the Warsaw bypass, where the A2 resumes and kilometric points are still relative to Świecko. A break occurs at Mińsk Mazowiecki, as the stretch from there on east was opened earlier than the connection from Warsaw.

With the completion of motorways or expressways replacing national roads, some of the national roads were remumbered (1 to 91, 2 to 92, 4 to 94). Where they follow the exact course of the “old” national road, kilometric point numbering was kept as-is, including rerouted sections and sections shared with other roads. For this reason, the section shared between DK73 and DK94 follows the numbering of DK94 (formerly DK4).

### Data quality

Data quality is not always perfect, therefore results of this tool need to be taken with a grain of salt.

WGS84 coordinates, if specified, generally seem to refer to the point indicated by `km` (the lower kilometric point). However, accuracy varies greatly and they can be up to several hundreds of meters off.

Where events refer to a section of road along which the road number, or kilometric point reference, changes, they are usually split. For example, an event affecting the A2 west of Konotopa and continuing on the S2 south of Warsaw would be split in one event for the A2 up to Konotopa, and another for the S2 from there onwards. An event affecting the S8 around the Zambrów bypass (which starts at km 0) would be split in three: one for the S8 up to the beginning of the bypass, another for the bypass and another for the section after the bypass. In these cases, adding up `km` and `dl` for the event will give the kilometric point for the other end of the location. However, this does not always seem to be the case: for example, there have been reports for the DK2b (which becomes the A2 around km 3) with a length of 30 km.

Occasionally, messages do not reflect recent renumberings. For example, the S12k originally ran from Jastków (where the expressway began at the time) up to Lublin-Felin. It was renumbered between 2017 and 2020, taking the numbering of the S12s starting at Zarzecze, after the connection between the two sections was completed. However, GDDKiA kept using the S12k reference and kilometric points in traffic reports as late as 2020.

Bottom line: take the information with a grain of salt, as it is not always accurate. If the data is at odds with what you are seeing on OSM, the best thing to do is to go there yourself and verify (or ask someone from the area to look at it). Otherwise, your best bet is to check some ground imagery from Mapillary, KartaView and friends, after making sure it is recent. If you’re lucky, you may be able to compare imagery taken at different times to find out if kilometric points have been remumbered.

### Licensing

Using traffic bulletins to validate OSM data and using the validation results to update OSM may result in the updated OSM data being considered a derivative work of the traffic bulletin by GDDKiA. Therefore, the license of the GDDKiA data is important. The traffic data is linked from https://dane.gov.pl, where it is stated to be under the [CC-0 1.0](https://creativecommons.org/publicdomain/zero/1.0/legalcode.pl) license. As CC-0 allows reuse without restrictions, there should not be an issue with use of the data in OSM.
